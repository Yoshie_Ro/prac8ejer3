package ito.poo.clases;

public class Periodo {
	
	private String tiempoCosecha;
	private float cantCosechaxtiempo;
	/*****************************/
	public Periodo() {
		super();
	}
	
	public Periodo(String tiempoCosecha, float cantCosechaxtiempo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}
	/*****************************/
	public String getTiempoCosecha() {
		return tiempoCosecha;
	}

	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}

	float getCantCosechaXTiempo() {
		return cantCosechaxtiempo;
	}

	public void setCantCosechaxtiempo(float cantCosechaxtiempo) {
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}
	/*****************************/
	@Override
	public String toString() {
		return "(Tiempo de cosecha: " + tiempoCosecha + ", Cantidad de cosecha: " + cantCosechaxtiempo + ")";
	}
	/*****************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cantCosechaxtiempo);
		result = prime * result + ((tiempoCosecha == null) ? 0 : tiempoCosecha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodo other = (Periodo) obj;
		if (Float.floatToIntBits(cantCosechaxtiempo) != Float.floatToIntBits(other.cantCosechaxtiempo))
			return false;
		if (tiempoCosecha == null) {
			if (other.tiempoCosecha != null)
				return false;
		} else if (!tiempoCosecha.equals(other.tiempoCosecha))
			return false;
		return true;
	}
	
	public int compareTo(Periodo arg0) {
		int r=0;
		if (!this.tiempoCosecha.equals(arg0.getTiempoCosecha()))
				return this.tiempoCosecha.compareTo(arg0.getTiempoCosecha());
		else if (this.cantCosechaxtiempo != arg0.getCantCosechaXTiempo())
			return this.cantCosechaxtiempo>arg0.getCantCosechaXTiempo()?1:-1;
		return r;
	}
}